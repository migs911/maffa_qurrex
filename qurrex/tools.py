import binascii

def ByteToHex(byteStr):

    return binascii.hexlify(byteStr)

def HexToByte(hexStr):

    return bytearray.fromhex(hexStr)
