import asyncio
from .protocol import TcpProtocol
from qurrex.log import *
from qurrex.tools import *
from qurrex.messages import *


class QurrexSession(TcpProtocol):

    def __init__(self, ip, port):
        TcpProtocol.__init__(self, ip, port)
        self.sequence_out = 0

    async def connect(self):
        await self.connection_made()

    async def message_writer(self, msg):
        frame = Frame()
        frame.msgId, frame.msgSize, frame.msgSeq = msg._msgid, msg.get_size(), self.sequence_out
        data = frame.pack() + msg.pack()
        logger.debug('\n%s'% ByteToHex(data))
        await self.data_writer(data)
        self.sequence_out += 1

    def message_received(self, frame, data):
        logger.debug(ByteToHex(data))
        try:
            if frame.msgId == RejectReport._msgid:
                self.onRejectReport(data)
            elif frame.msgId == NewOrderReport._msgid:
                self.onNewOrderReport(data)
            elif frame.msgId == CancelReport._msgid:
                self.onCancelReport(data)
            elif frame.msgId == MassCancelReport._msgid:
                self.onMassCancelReport(data)
            elif frame.msgId == ExecutionReport._msgid:
                self.onExecutionReport(data)
            else:
                logger.error("Unknown msgId %s"% frame.msgId)
        except Exception as e:
            logger.error(e)

    def onRejectReport(self, data):
        reject = RejectReport(data)
        logger.error('Error code: %s' % reject.error_code)

    def onNewOrderReport(self, data):
        pass

    def onCancelReport(self, data):
        pass

    def onMassCancelReport(self, data):
        pass

    def onExecutionReport(self, data):
        pass
