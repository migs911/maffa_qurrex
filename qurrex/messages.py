from ctypes import *


class BinaryStructure(Structure):
    _pack_ = 1

    def __str__(self):
        rows = []
        for field in self._fields_:
            value = getattr(self, field[0])
            if isinstance(value, bytes):
                value = value.decode()
            rows.append("{0}: {1}\n".format(field[0], value))
        if hasattr(self,'deals'):
            for item in self.deals:
                rows.append("deal:\n")
                for field in item._fields_:
                    value = getattr(item, field[0])
                    rows.append("  {0}: {1}\n".format(field[0], value))
        return ''.join(rows)

    def __init__(self, data=None):
        if data:
            self.unpack(data)

    def unpack(self, raw):
        fit = sizeof(self)
        memmove(addressof(self), raw[:fit], fit)

    def pack(self):
        return bytearray(self)[:]

    def get_size(self):
        return sizeof(self)

class Frame(BinaryStructure):

    _fields_ = [
    ('msgId', c_ushort),
    ('msgSize', c_ushort),
    ('msgSeq', c_longlong)]

class Deal(BinaryStructure):

    _fields_ = [
    ('deal_id', c_longlong),
    ('price', c_longlong),
    ('amount', c_longlong)]

class NewOrderRequest(BinaryStructure):
    _msgid = 1

    _fields_ = [
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('type', c_byte),
    ('time_in_force', c_byte),
    ('side', c_byte),
    ('auto_cancel', c_byte),
    ('amount', c_longlong),
    ('price', c_longlong),
    ('flags', c_longlong),
    ('comment', c_char * 16)]

class NewOrderReport(BinaryStructure):
    _msgid = 2

    _fields_ = [
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('type', c_byte),
    ('time_in_force', c_byte),
    ('side', c_byte),
    ('auto_cancel', c_byte),
    ('amount', c_longlong),
    ('price', c_longlong),
    ('flags', c_longlong),
    ('comment', c_char * 16),
    ('system_time', c_longlong),
    ('exchange_order_id', c_longlong),
    ('liquidity_pool_id', c_ushort),
    ('order_routing_rule', c_ushort)]

class CancelRequest(BinaryStructure):
    _msgid = 3

    _fields_ = [
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('exchange_order_id', c_longlong),
    ('order_trequst_id', c_char * 16),
    ('side', c_byte)]

class CancelReport(BinaryStructure):
    _msgid = 4

    _fields_ = [
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('exchange_order_id', c_longlong),
    ('order_trequst_id', c_char * 16),
    ('side', c_byte),
    ('system_time', c_longlong),
    ('amount_cancelled', c_int),
    ('amount_rest', c_int),
    ('cancel_reason', c_int)]

class MassCancelRequest(BinaryStructure):
    _msgid = 12

    _fields_ = [
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('cancel_mode', c_char)]

class MassCancelReport(BinaryStructure):
    _msgid = 14

    _fields_ = [
    ('system_time', c_longlong),
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('cancel_mode', c_byte),
    ('cancel_status', c_byte),
    ('cancelled_orders', c_int)]

class ExecutionReport(BinaryStructure):
    _msgid = 5

    _fields_ = [
    ('trequst_id', c_char * 16),
    ('clearing_id', c_char * 16),
    ('trader_id', c_char * 16),
    ('instrument', c_int),
    ('type', c_byte),
    ('time_in_force', c_byte),
    ('side', c_byte),
    ('auto_cancel', c_byte),
    ('amount', c_longlong),
    ('price', c_longlong),
    ('flags', c_longlong),
    ('comment', c_char * 16),
    ('system_time', c_longlong),
    ('exchange_order_id', c_longlong),
    ('amount_rest', c_longlong),
    ('deals_num', c_byte)]

class RejectReport(BinaryStructure):
    _msgid = 10

    _fields_ = [
    ('system_time', c_longlong),
    ('trequst_id',  c_char * 16),
    ('error_code', c_ushort)]
