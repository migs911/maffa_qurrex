import asyncio
import time
import uvloop
import logging
import ctypes
import decimal
import random
from decimal import *
from qurrex.tools import *
from qurrex.messages import *
from qurrex.async.session import QurrexSession

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
logging.basicConfig(
    format='%(asctime)-22s %(levelname)-5s  %(name)s.%(funcName)s: %(message)s',
    level=logging.INFO)
logger = logging.getLogger(__name__)


class ClassicClient(QurrexSession):

    def __init__(self, ip, port):
        QurrexSession.__init__(self, ip, port)
        self.clorder = 0

    def onRejectReport(self, data):
        report = RejectReport(data)
        logger.error('Error code: %s' % report.error_code)

    def onNewOrderReport(self, data):
        report = NewOrderReport(data)
        logger.info('\n%s' % report)

    def onCancelReport(self, data):
        report = CancelReport(data)
        logger.info('\n%s' % report)

    def onMassCancelReport(self, data):
        report = MassCancelReport(data)
        logger.info('\n%s' % report)

    def onExecutionReport(self, data):
        report = ExecutionReport(data)
        main = report.get_size()
        entry = ctypes.sizeof(Deal())
        deals = []
        for x in range(report.deals_num):
            deal = Deal(data[main:main+entry])
            deals.append(deal)
            main += entry
        report.deals = deals
        logger.info('\n%s' % report)

    async def feed(self):
        while True:
            self.clorder += 1
            order = NewOrderRequest()
            order.trequst_id = str(self.clorder).encode()
            order.clearing_id = "CLEARING".encode()
            order.trader_id = "Client01".encode()
            order.instrument = 5
            order.type = random.randint(1,2)
            order.time_in_force = random.randint(1,3)
            order.side = random.randint(1,2)
            order.auto_cancel = 1
            order.amount = Decimal('100000000.0')
            order.price = Decimal('100000000.0')
            order.flags = 0
            order.comment = "testing".encode()
            await self.message_writer(order)
            await asyncio.sleep(10)
            

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    session = ClassicClient('IP', 5001)
    loop.run_until_complete(session.connect())
    loop.run_until_complete(session.feed())
    loop.run_forever()
